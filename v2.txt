GESCHÄFTSFÜHRERDIENSTVERTRAG
Zwischen
Immobilien Verwaltung Göttingen GmbH, Zur Mühle
2, 37077 Göttingen, vertreten durch Matthias Michaelsen,
dieser vertreten durch Leon Michaelsen
- im Folgenden „Gesellschaft” genannt –

und
Friedrich Brune, wohnhaft in Burgstraße 1, 37085 Göttingen
- im Folgenden „Geschäftsführer” genannt- zusammen die „Parteien“ –
wird der folgende Geschäftsführerdienstvertrag geschlossen:

Vertrag 2

§ 1 Position, Tätigkeit sowie Geschäftsführungs- und Vertretungsbefugnisse
(1) Friedrich Brune wurde mit Beschluss der Gesellschafterversammlung vom 2. September 2019
mit Wirkung ab Unterzeichnung dieses Vertrages zum Geschäftsführer der Gesellschaft bestellt.
(2) Der Geschäftsführer führt die Geschäfte der Gesellschaft mit der Sorgfalt eines ordentlichen
Kaufmanns nach Maßgabe der Gesetze, des Gesellschaftsvertrages, der Richtlinien der Gesellschaft sowie den Weisungen der Gesellschafterversammlung.
(3) Der Geschäftsführer vertritt die Gesellschaft nach außen hin satzungsgemäß und gemäß Bestel-

lungsbeschluss durch die Gesellschafter. Soweit dem Geschäftsführer Alleinvertretungsbefugnis
erteilt worden ist, sind die Gesellschafter berechtigt, dies dahingehend zu ändern, dass der Geschäftsführer künftig die Gesellschaft gemeinsam mit einem und/oder mehreren anderen Geschäftsführer(n) oder auch mit einem Prokuristen vertritt.
(4) Der Geschäftsführer ist von den Beschränkungen des § 181 BGB nicht befreit.
(5) Der Geschäftsführer ist nach Maßgabe dieses Dienstvertrags für den Bereich Finanzen zuständig
und verantwortlich. Die Gesellschaft behält sich vor, die Zuständigkeit und Verantwortlichkeit
zu erweitern, zu vermindern und auch zu ändern. Die Gesellschaft ist auch berechtigt, weitere
Geschäftsführer zu bestellen.
§ 2 Arbeitszeit und Arbeitsort
(1) Der Geschäftsführer wird seine volle Arbeitskraft in den Dienst der Gesellschaft stellen. In der
Bestimmung der täglichen Arbeitszeit ist er frei.
(2) Arbeitsort ist Zur Mühle 2, 37077 Göttingen.
§ 3 Vergütung
(1) Als Vergütung für seine Tätigkeit nach diesem Vertrag erhält der Geschäftsführer ein Jahresgehalt
von brutto € 60.000
(in Worten: € Sechzigtausend) zahlbar in zwölf gleichen Raten zum Monatsende.
(2) Über das Grundgehalt hinaus kann die Gesellschaft leistungsabhängige Bonuszahlungen und
weitere Sonderleistungen gewähren. Diese Sonderleistungen sind freiwillig. Auch bei mehrmaliger Leistung entsteht kein Anspruch auf Leistung für die Zukunft. Die Gesellschaft behält sich
vor, einzelne Leistungen nicht weiter zu gewähren oder deren Inhalt zu ändern. Etwaige auf die
Sonderleistungen anfallende Steuern trägt der Geschäftsführer.
(3) Mit der Zahlung des Grundgehaltes sind alle Leistungen des Geschäftsführers nach diesem Vertrag abgegolten. Dies gilt insbesondere für Arbeiten außerhalb der üblichen Bürozeiten, Übernahme von weiteren Funktionen innerhalb der Gesellschaft bzw. verbundener Unternehmen einschließlich etwaiger Aufsichtsratsmandate, Diensterfindungen und urheberrechtlicher Werke.
(4) Im Falle von Überzahlungen kann sich der Geschäftsführer nicht auf den Wegfall der Bereicherung gemäß § 818 Abs. 3 BGB berufen.

2

Vertrag 2

(5) Über die Höhe der Vergütung haben die Parteien Stillschweigen zu bewahren.
§ 4 Dienstfahrzeug
(1) Der Geschäftsführer erhält ein Dienstfahrzeug, welches er auch privat nutzen darf, vorausgesetzt,
dass hierfür ein gesonderter Kfz-Überlassungsvertrag abgeschlossen wurde, der diesem Dienstvertrag als Anlage 1 beigefügt ist. Die Nutzung des Dienstfahrzeugs erfolgt ausschließlich entsprechend den Regelungen dieses Dienstvertrags und des Kfz-Überlassungsvertrags. Der geldwerte Vorteil der Privatnutzung liegt deutlich unter 25% der Vergütung des Geschäftsführers nach
diesem Vertrag.
(2) Die private Nutzung kann von der Gesellschaft widerrufen werden, wenn der Geschäftsführer
das Dienstfahrzeug vertragswidrig benutzt oder wenn der Dienstvertrag gekündigt worden ist
und wenn die Gesellschaft den Geschäftsführer berechtigt von seiner Verpflichtung zur Tätigkeit
für die Gesellschaft freigestellt oder suspendiert hat. Ein Anspruch des Geschäftsführers wegen
des Entzugs der privaten Nutzung besteht in diesen Fällen nicht.
(3) Die Versteuerung des mit der privaten Nutzung verbundenen geldwerten Vorteils erfolgt nach
den jeweils geltenden steuerlichen Regelungen im Rahmen des Lohnsteuerabzugsverfahrens und
erfolgt zu Lasten des Geschäftsführers.
(4) Im Übrigen gelten die Regelungen des Kfz-Überlassungsvertrags.
§ 5 Erstattung von Kosten
(1) Reisekosten und andere notwendige Auslagen des Geschäftsführers für dienstliche Zwecke werden gemäß der jeweils gültigen Reisekostenregelung der Gesellschaft sowie den jeweils gültigen
steuerlichen Bestimmungen gegen ordnungsgemäßen Nachweis erstattet.
(2) Für Dienstreisen kann der Geschäftsführer das Dienstfahrzeug, soweit erforderlich, auch einen
Leihwagen, die Bahn oder das Flugzeug benutzen. Bei Fahrten mit der Bahn werden Fahrten der
ersten Klasse erstattet, bei Nutzung des Flugzeugs Tickets der Economy-Klasse und der Business-Klasse für Flüge über sechs Stunden Dauer. Leihwagen können in der Größenordnung des
Firmenwagens angemietet werden.
(3) Die Gesellschaft zahlt an den Geschäftsführer monatlich einen Zuschuss in Höhe von € 60 brutto
für seinen privaten Telefonanschluss, den er auch geschäftlich nutzt.
§ 6 Arbeitsverhinderung, Erkrankung
(1) Der Geschäftsführer ist verpflichtet, die Gesellschaft von jeder Arbeitsverhinderung, ihrer voraussichtlichen Dauer und anstehenden dringenden Arbeiten unverzüglich zu unterrichten. Auf
Verlangen hat er der Gesellschaft die Gründe für die Arbeitsverhinderung mitzuteilen.
(2) Im Falle einer Arbeitsunfähigkeit wegen Krankheit ist der Geschäftsführer verpflichtet, spätestens bis nach drei Tagen der Gesellschaft eine ärztliche Bescheinigung über die Arbeitsverhinderung und ihre voraussichtliche Dauer vorzulegen. Dauert die Arbeitsunfähigkeit länger als in der
Bescheinigung angegeben, ist der Geschäftsführer verpflichtet, innerhalb von drei Tagen eine
3

Vertrag 2

neue ärztliche Bescheinigung einzureichen. Auch in diesem Fall muss der Geschäftsführer der
Gesellschaft unverzüglich unterrichten, falls die Dauer der Krankheit die in der Folgebescheinigung angegebene Dauer überschreitet.
(3) Wird der Geschäftsführer an seiner Arbeitsleistung infolge Krankheit verhindert, ohne dass ihn
ein Verschulden trifft, so behält er seinen Anspruch auf Zahlung des Grundgehalts für die Zeit
der Arbeitsunfähigkeit bis zur Dauer von 10 Monaten, längstens jedoch bis zum Ende dieses
Vertrages.
§ 7 Urlaub
(1) Dem Geschäftsführer steht kalenderjährlich ein Urlaub von 30 Arbeitstagen zu. Als Arbeitstage
gelten alle Kalendertage mit Ausnahme von Samstagen, Sonntagen und gesetzlichen Feiertagen.
(2) Bei der zeitlichen Festlegung des Urlaubs hat der Geschäftsführer die geschäftlichen Belange der
Gesellschaft zu berücksichtigen. Lage und Dauer des Urlaubs ist mit den etwaigen weiteren Geschäftsführern sowie mit den Gesellschaftern abzustimmen.
(3) Im Kalenderjahr nicht genommener Urlaub wird auf das Folgejahr übertragen. Wird der übertragene Urlaub nicht bis spätestens zum 1. Dezember des Folgejahres genommen, so verfällt dieser
ersatzlos.
§ 8 Nebentätigkeiten
(1) Der Geschäftsführer wird ausschließlich für die Gesellschaft tätig. Die Aufnahme oder Fortführung einer weiteren selbständigen oder unselbständigen Tätigkeit sowie die Übernahme von Ehrenämtern und Aufsichtsratsmandaten bedürfen der vorherigen schriftlichen Zustimmung der
Gesellschaft, sofern die Tätigkeit entgeltlich erfolgt oder üblicherweise nur gegen Entgelt übernommen wird oder die Leistungsfähigkeit des Geschäftsführers einschränkt. Die Gesellschaft
wird die Zustimmung erteilen, sofern die weitere Tätigkeit die Interessen der Gesellschaft nicht
nachteilig berührt. Die Zustimmung kann jederzeit widerrufen werden, wenn dies betriebliche
Belange der Gesellschaft erfordern. Jede Veränderung der Art oder des Umfangs der Nebentätigkeit ist der Gesellschaft durch den Geschäftsführer unverzüglich anzuzeigen.
(2) Auf Verlangen der Gesellschaft wird der Geschäftsführer Aufsichtsrats-, Beirats- oder Verwaltungsratsmandate sowie Ehrenämter und sonstige Ämter übernehmen, sofern dies im Interesse
der Gesellschaft erforderlich und dem Geschäftsführer zumutbar ist. Im Falle der Beendigung
dieses Vertrages hat der Geschäftsführer diese Mandate und Ehrenämter auf Verlangen der Gesellschaft niederzulegen, soweit dies gesetzlich möglich ist und sich der Geschäftsführer hierdurch nicht schadensersatzpflichtig macht.
§ 9 Verschwiegenheit
(1) Der Geschäftsführer ist verpflichtet, über alle Betriebs- und Geschäftsgeheimnisse der Gesellschaft sowie der mit ihr verbundenen Unternehmen strenges Stillschweigen zu bewahren. Hierzu
zählen insbesondere: Projektbeschreibungen und laufende Angebote, Know-how der Gesell-

4

Vertrag 2

schaft, Kenntnisse über die von der Gesellschaft angewandten Methoden, Preisbildung und Gewinnmargen, Zeichnungen, Testergebnisse, Muster, Businesspläne, Erfindungen sowie Namen
der Kunden und Lieferanten der Gesellschaft.
(2) Darüber hinaus ist der Geschäftsführer verpflichtet, Stillschweigen über solche Informationen zu
bewahren, die ihm von den Gesellschaftern, Organmitgliedern oder Mitarbeitern der Gesellschaft
ausdrücklich oder konkludent als vertraulich mitgeteilt wurden oder ihm auf sonstige Weise als
vertraulich bekannt wurden oder deren Vertraulichkeit ohne weiteres erkennbar ist.
(3) Diese Verschwiegenheitspflicht bleibt auch nach Beendigung des Vertrages wirksam. Sie entfällt,
wenn und soweit die betreffenden Informationen nachweislich allgemein bekannt geworden sind
oder eine gesetzlich oder richterlich oder behördlich angeordnete Pflicht zur Offenlegung besteht.
§ 10 Wettbewerbsverbot
(1) Dem Geschäftsführer ist es untersagt, während der Dauer dieses Vertrages in selbstständiger,
unselbstständiger oder in sonstiger Weise für ein Unternehmen tätig zu werden, das mittelbar
oder unmittelbar in Wettbewerb zur Gesellschaft oder mit ihr verbundenen Unternehmen steht.
Insbesondere darf der Geschäftsführer kein konkurrierendes Handelsgewerbe eröffnen oder betreiben beziehungsweise sich an einem solchen beteiligen.
(2) Hiervon ausgenommen sind bloße Finanzbeteiligungen, die keine unternehmerischen Einflussmöglichkeiten eröffnen. Der Geschäftsführer wird das Bestehen solcher Beteiligungen jedoch
der Gesellschaft unverzüglich mitteilen.
(3) Für jeden Fall des Verstoßes gegen dieses Wettbewerbsverbot durch den Geschäftsführer kann
die Gesellschaft Zahlung einer Vertragsstrafe in Höhe von € 25.000 verlangen. Besteht die Verletzungshandlung in der Eingehung eines Dauerschuldverhältnisses (z.B. auf Grund eines Arbeits- oder Dienstvertrages), in dem Betreiben eines Wettbewerbsunternehmens oder in der kapitalmäßigen Beteiligung an einem solchen Unternehmen, wird die Vertragsstrafe für jeden angefangenen Monat, in dem die konkrete Verletzungshandlung besteht, neu verwirkt (Dauerverstoß). Mehrere Verletzungshandlungen lösen jeweils gesonderte Vertragsstrafen aus, gegebenenfalls auch mehrfach innerhalb eines Monats. Erfolgen dagegen einzelne Verletzungshandlungen
im Rahmen eines Dauerverstoßes, sind sie von der für den Dauerverstoß verwirkten Vertragsstrafe umfasst.
(4) Die Geltendmachung eines über die vorgenannte Vertragsstrafe hinausgehenden Schadens bleibt
der Gesellschaft ebenso vorbehalten wie die Geltendmachung etwa von Unterlassungsansprüchen.
§ 11 Nachträgliches Wettbewerbsverbot
(1) Dem Geschäftsführer ist es nicht gestattet, für die Dauer von 12 Monaten nach dem Ende seines
Arbeitsverhältnisses in selbständiger, unselbständiger oder anderer Weise für Dritte tätig zu werden, die mit der Gesellschaft in unmittelbarem oder mittelbaren Wettbewerb stehen. Ebenso ist

5

Vertrag 2

es dem Geschäftsführer nicht gestattet, während der Laufzeit des Wettbewerbsverbotes ein solches Unternehmen zu errichten, zu erwerben oder sich hieran unmittelbar oder mittelbar zu beteiligen.
(2) Das Wettbewerbsverbot wirkt auch im Hinblick auf die mit der Gesellschaft jetzt und in Zukunft
verbundenen Unternehmen.
(3) Das Wettbewerbsverbot gilt auch für und gegen einen Rechtsnachfolger der Gesellschaft. Bei
einer Veräußerung des Betriebes geht es auf den Erwerber über. Der Geschäftsführer erklärt sich
mit dem Übergang der Rechte aus dieser Vereinbarung auf einen etwaigen Rechtsnachfolger ausdrücklich einverstanden.
(4) Für die Dauer des Wettbewerbsverbots zahlt die Gesellschaft eine Karenzentschädigung, die für
jeden Monat des Verbots der Hälfte der vom Geschäftsführer zuletzt bezogenen vertragsmäßigen
Leistungen der Gesellschaft entspricht. Eine Karenzentschädigung ist jedoch nicht zu zahlen
während der Absolvierung des Wehrdienstes, Zivildienstes oder der Verbüßung einer Freiheitsstrafe.
(5) Anderweitigen Erwerb muss sich der Geschäftsführer gem. § 74 c HGB auf die Entschädigung
anrechnen lassen. Der Geschäftsführer hat unaufgefordert mitzuteilen, ob und in welcher Höhe
er neben der Karenzentschädigung Vergütungen aus anderweitiger Verwertung seiner Arbeitskraft bezieht. Auf Verlangen sind die Angaben durch Vorlage prüfbarer Unterlagen zu belegen.
(6) § 10 Abs. 3 dieses Vertrages gilt für das nachvertragliche Wettbewerbsverbot entsprechend.
(7) Das nachvertragliche Wettbewerbsverbot tritt nicht in Kraft, wenn dieser Dienstvertrag aufgrund
Eintritts des Geschäftsführers in den Ruhestand, wegen Invalidität des Geschäftsführers oder
nach Erreichen des gesetzlichen Renteneintrittsalters endet. Das Wettbewerbsverbot und die Verpflichtung zur Zahlung einer Karenzentschädigung nach § 11 Abs. 5 enden in jedem Fall mit
Eintritt des zuletzt genannten Zeitpunkts.
(8) Die Gesellschaft kann zudem vor Ende dieses Dienstvertrags durch schriftliche Erklärung auf
das nachvertragliche Wettbewerbsverbot mit der Wirkung verzichten, dass sie mit Ablauf von
sechs Monaten seit der Erklärung von der Verpflichtung frei wird, die Karenzentschädigung zu
zahlen.
(9) Im Übrigen gelten die §§ 74 ff. HGB entsprechend.
§ 12 Gewerbliche Schutzrechte
(1) Der Geschäftsführer räumt der Gesellschaft alle Rechte an den von ihm nach diesem Geschäftsführerdienstvertrag erzielten Arbeitsergebnissen ein, soweit diese nach den gesetzlichen Vorschriften übertragbar sind. Die Rechte stehen ausschließlich und ohne zeitliche, örtliche oder
sachliche Beschränkung der Gesellschaft zu.
(2) Der Geschäftsführer räumt der Gesellschaft ein ausschließliches, zeitlich, örtlich und sachlich
unbeschränktes Nutzungsrecht an den unter das Urheberrecht fallenden Arbeitsergebnissen ein,

6

Vertrag 2

soweit diese aus der Tätigkeit des Geschäftsführers für die Gesellschaft entstehen oder maßgeblich auf Erfahrungen oder Arbeiten für die Gesellschaft oder dabei erlangten Unterlagen beruhen.
(3) Das Nutzungsrecht nach Abs. 2 umfasst alle Nutzungsarten, seien sie bekannt oder noch unbekannt, jetzt oder zukünftig. Dazu gehört auch das Recht, das Werk zu bearbeiten, zu verändern,
zu vervielfältigen, zu veröffentlichen und zu verwerten.
(4) Eine Verpflichtung zur Autorernennung besteht nicht. Das Zugangsrecht des Geschäftsführers zu
den von ihm geschaffenen Werken bzw. Werkteilen sowie sonstigen Arbeitsergebnissen und das
Recht auf Herausgabe einer Autorenkopie werden ausgeschlossen.
(5) Die Gesellschaft ist ferner ohne gesonderte Zustimmung befugt, die Rechte an den Arbeitsergebnissen ganz oder teilweise auf Dritte zu übertragen oder Dritten Nutzungsrechte einzuräumen.
(6) Die Einräumung der Rechte nach Abs. 1 bis 5 ist mit der Vergütung des Geschäftsführers gemäß
§ 3 dieses Geschäftsführerdienstvertrages abgegolten.
(7) Die Vereinbarungen nach Abs. 1 bis 6 behalten auch nach Beendigung des Vertragsverhältnisses
ihre Gültigkeit.
§ 13 Unterlagen und Gegenstände
(1) Der Geschäftsführer hat sämtliche im Rahmen der Erbringung seiner Leistungen nach diesem
Vertrag erlangten, die Gesellschaft betreffenden Gegenstände und Unterlagen sicher zu verwahren und durch geeignete Maßnahmen vor dem unbefugten Zugriff Dritter zu schützen.
(2) Nach Beendigung dieses Vertrages oder auf Aufforderung durch die Gesellschaft sind sämtliche
Gegenstände und Unterlagen einschließlich aller Datenträger, die der Geschäftsführer im Rahmen der Erbringung seiner Leistungen nach diesem Vertrag erlangt hat, unverzüglich an die Gesellschaft herauszugeben.
(3) Auf Verlangen der Gesellschaft hat der Geschäftsführer die Vollständigkeit der herausgegebenen
Gegenstände und Unterlagen schriftlich zu bestätigen.
(4) Ein Zurückbehaltungsrecht an den Gegenständen und Unterlagen der Gesellschaft steht dem Geschäftsführer nicht zu.
§ 14 Vertragslaufzeit
(1) Dieser Vertrag beginnt am 2. September 2019 und wird für die Dauern von fünf Jahren geschlossen. Er verlängert sich jeweils um 2 Jahre, wenn er nicht mit einer Frist von 6 Monaten vor dem
jeweiligen Vertragsende gekündigt wird.
(2) Der Vertrag endet jedoch spätestens mit Ablauf des Monats, in dem der Geschäftsführer die Voraussetzungen für den Bezug der gesetzlichen Altersrente erfüllt, ohne dass es einer Kündigung
und/oder Beendigung bedarf.
(3) Der Vertrag kann von beiden Seiten mit einer Kündigungsfrist von sechs zum Monatsende gekündigt werden.

7

Vertrag 2

(4) Das Recht der Parteien zur fristlosen Kündigung des Vertrages aus wichtigem Grund bleibt unberührt.
(5) Die Gesellschaft ist berechtigt, den Geschäftsführer im Falle einer Kündigung dieses Vertrages
jederzeit bis zum Ablauf der Kündigungsfrist von seiner Pflicht zur Arbeitsleistung freizustellen.
In diesem Fall bleibt sie zur Zahlung der vertragsmäßigen Vergütung bis zum Ablauf der Kündigungsfrist verpflichtet. Etwaige andere Einkünfte, die der Geschäftsführer während der Freistellung aus der Verwendung seiner Arbeitskraft erzielt oder böswillig zu erzielen unterlässt, werden
gemäß § 615 Satz 2 BGB auf die zu zahlende Vergütung angerechnet. Auf die Zeit der Freistellung werden etwaige ausstehende Urlaubsansprüche angerechnet, sofern die Freistellung unwiderruflich erfolgt.
(6) Die Kündigung bedarf zu ihrer Wirksamkeit der Schriftform.
§ 15 Zusicherung
(1) Der Geschäftsführer sichert zu, dass er nicht durch ein vertragliches oder nachvertragliches Wettbewerbsverbot an der Tätigkeit für die Gesellschaft gehindert ist.
§ 16 Schlussbestimmungen
(1) Dieser Vertrag stellt die vollständige Vereinbarung der Parteien dar. Weitere mündliche oder
schriftliche Vereinbarungen bezüglich des Vertragsgegenstands wurden nicht getroffen.
(2) Änderungen, Ergänzungen und die Aufhebung dieses Dienstvertrags bedürfen zu ihrer Wirksamkeit der Schriftform. Dies gilt auch für die Änderung dieser Schriftformklausel selbst. Ausgeschlossen sind damit insbesondere Änderungen dieses Dienstvertrags durch betriebliche Übung.
Das vorstehende Schriftformerfordernis findet keine Anwendung bei Abreden, die nach Abschluss dieses Dienstvertrags unmittelbar zwischen den Vertragsparteien mündlich getroffen
werden.
(3) Dieser Vertrag ersetzt alle früheren Vereinbarungen der Parteien bezüglich des Vertragsgegenstands.
(4) Dieser Vertrag unterliegt dem Recht der Bundesrepublik Deutschland mit Ausnahme des inter-

nationalen Privatrechts.
(5) Sollte eine Bestimmung dieses Vertrages unwirksam sein oder unwirksam werden, so bleibt die
Wirksamkeit des Vertrages im Übrigen unberührt. Die Vertragsparteien sind in diesem Falle verpflichtet, anstelle der unwirksamen Bestimmung eine wirksame Regelung zu treffen, die den
wirtschaftlichen Zielen dieses Vertrages möglichst nahe kommt.
(6) Ausschließlicher Gerichtsstand für alle Streitigkeiten aus oder im Zusammenhang mit dieser Vereinbarung oder ihrer Wirksamkeit ist Göttingen.

8


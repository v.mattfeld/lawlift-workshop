3,4c3,5
< Frisöre Kaulitz GmbH, Bergstraße 17, 37075 Göttingen,
< vertreten durch Ann-Kathrin Kaulitz
---
> Thompson O`Neil Murray Capital Investment GmbH,
> Schlossallee 211, 30177 Hannover, vertreten durch Mark
> Caldwell
8c9
< Vivien Böck, wohnhaft in Göthe-Straße 98, 37073 Göttingen
---
> Justus Maximilian von Taldorf, wohnhaft in Theaterstraße 3, 30175 Hannover
17c18
< (1) Vivien Böck wurde mit Beschluss der Gesellschafterversammlung vom 3. Oktober 2019 mit Wirkung ab Unterzeichnung dieses Vertrages zum Geschäftsführer der Gesellschaft bestellt.
---
> (1) Justus Maximilian von Taldorf wurde mit Beschluss der Gesellschafterversammlung vom 9. September 2019 mit Wirkung ab Unterzeichnung dieses Vertrages zum Geschäftsführer der Gesellschaft bestellt.
24a26,27
> (5) Der Geschäftsführer ist nach Maßgabe dieses Dienstvertrags für den Bereich Venture Capital
> zuständig und verantwortlich. Die Gesellschaft behält sich vor, die Zuständigkeit und Verantwortlichkeit zu erweitern, zu vermindern und auch zu ändern. Die Gesellschaft ist auch berechtigt, weitere Geschäftsführer zu bestellen.
26,28c29,37
< (1) Der Geschäftsführer wird seine volle Arbeitskraft in den Dienst der Gesellschaft stellen. In der
< Bestimmung der täglichen Arbeitszeit ist er frei.
< (2) Arbeitsort ist Bergstraße 17, 37075 Göttingen.
---
> (1) Der Geschäftsführer hat seine volle Schaffenskraft sowie sein ganzes Wissen und Können in die
> 
> Dienste der Gesellschaft zu stellen. Er ist nicht an die Einhaltung bestimmter Zeiten zur Erbringung seiner Tätigkeit oder einen bestimmten Arbeitsort gebunden, soll aber grundsätzlich zu den
> betrieblichen Arbeitszeiten tätig und im Betrieb anwesend oder erreichbar sein, soweit er nicht
> Dienstreisen oder ähnliches unternimmt. Er ist jedoch verpflichtet, jederzeit, soweit dies das
> Wohl der Gesellschaft erfordert, zu ihrer Verfügung zu stehen und ihre Interessen wahrzunehmen.
> Er ist dabei verpflichtet, jederzeit, auch an Samstagen, Sonn- und Feiertagen, für die Gesellschaft
> tätig zu werden, sofern und soweit dies die geschäftlichen Belange der Gesellschaft erfordern.
> (2) Arbeitsort ist Schlossallee 211, 30177 Hannover.
31,32c40,41
< von brutto € 18.000
< (in Worten: € Achtzehntausend) zahlbar in zwölf gleichen Raten zum Monatsende.
---
> von brutto € 1.200.000
> (in Worten: € Einemillionenundzweihunderttausend) zahlbar in zwölf gleichen Raten zum Monatsende.
35,36c44,49
< 25 ‰
< (in Worten: ‰ Fünfundzwanzig)
---
> 2.5 ‰
> (in Worten: ‰ Zweikommafünf).
> 2
> 
> Vertrag 1
> 
42,46d54
< 
< 2
< 
< Vertrag 1
< 
54c62,80
< § 4 Erstattung von Kosten
---
> § 4 Dienstfahrzeug
> (1) Der Geschäftsführer erhält ein Dienstfahrzeug, welches er auch privat nutzen darf, vorausgesetzt,
> dass hierfür ein gesonderter Kfz-Überlassungsvertrag abgeschlossen wurde, der diesem Dienstvertrag als Anlage 1 beigefügt ist. Die Nutzung des Dienstfahrzeugs erfolgt ausschließlich entsprechend den Regelungen dieses Dienstvertrags und des Kfz-Überlassungsvertrags. Der geldwerte Vorteil der Privatnutzung liegt deutlich unter 25% der Vergütung des Geschäftsführers nach
> diesem Vertrag.
> (2) Die private Nutzung kann von der Gesellschaft widerrufen werden, wenn der Geschäftsführer
> das Dienstfahrzeug vertragswidrig benutzt oder wenn der Dienstvertrag gekündigt worden ist
> und wenn die Gesellschaft den Geschäftsführer berechtigt von seiner Verpflichtung zur Tätigkeit
> für die Gesellschaft freigestellt oder suspendiert hat. Ein Anspruch des Geschäftsführers wegen
> des Entzugs der privaten Nutzung besteht in diesen Fällen nicht.
> (3) Die Versteuerung des mit der privaten Nutzung verbundenen geldwerten Vorteils erfolgt nach
> den jeweils geltenden steuerlichen Regelungen im Rahmen des Lohnsteuerabzugsverfahrens und
> erfolgt zu Lasten des Geschäftsführers.
> (4) Im Übrigen gelten die Regelungen des Kfz-Überlassungsvertrags.
> 
> 3
> 
> Vertrag 1
> 
> § 5 Erstattung von Kosten
59,60c85,89
< ersten Klasse erstattet, bei Nutzung des Flugzeugs Tickets der Economy-Klasse und der Business-Klasse für Flüge über sechs Stunden Dauer.
< § 5 Arbeitsverhinderung, Erkrankung
---
> ersten Klasse erstattet, bei Nutzung des Flugzeugs Tickets der Economy-Klasse und der Business-Klasse für Flüge über sechs Stunden Dauer. Leihwagen können in der Größenordnung des
> Firmenwagens angemietet werden.
> (3) Die Gesellschaft zahlt an den Geschäftsführer monatlich einen Zuschuss in Höhe von € 250
> brutto für seinen privaten Telefonanschluss, den er auch geschäftlich nutzt.
> § 6 Arbeitsverhinderung, Erkrankung
63,66c92,95
< (2) Im Falle einer Arbeitsunfähigkeit wegen Krankheit ist der Geschäftsführer verpflichtet, der Gesellschaft eine ärztliche Bescheinigung über die Arbeitsverhinderung und ihre voraussichtliche
< Dauer vorzulegen. Dauert die Arbeitsunfähigkeit länger als in der Bescheinigung angegeben, ist
< der Geschäftsführer verpflichtet, innerhalb von drei Tagen eine neue ärztliche Bescheinigung
< einzureichen. Auch in diesem Fall muss der Geschäftsführer der Gesellschaft unverzüglich unterrichten, falls die Dauer der Krankheit die in der Folgebescheinigung angegebene Dauer überschreitet.
---
> (2) Im Falle einer Arbeitsunfähigkeit wegen Krankheit ist der Geschäftsführer verpflichtet, spätestens nach fünf Tagen der Gesellschaft eine ärztliche Bescheinigung über die Arbeitsverhinderung
> und ihre voraussichtliche Dauer vorzulegen. Dauert die Arbeitsunfähigkeit länger als in der Bescheinigung angegeben, ist der Geschäftsführer verpflichtet, innerhalb von drei Tagen eine neue
> ärztliche Bescheinigung einzureichen. Auch in diesem Fall muss der Geschäftsführer der Gesellschaft unverzüglich unterrichten, falls die Dauer der Krankheit die in der Folgebescheinigung
> angegebene Dauer überschreitet.
70c99
< der Arbeitsunfähigkeit bis zur Dauer von sechs Monaten, längstens jedoch bis zum Ende dieses
---
> der Arbeitsunfähigkeit bis zur Dauer von 24 Monaten, längstens jedoch bis zum Ende dieses
72,76d100
< 
< 3
< 
< Vertrag 1
< 
79,81c103,105
< länger als zehn Wochen seine Tätigkeit nicht erbringt.
< § 6 Urlaub
< (1) Dem Geschäftsführer steht kalenderjährlich ein Urlaub von 20 Arbeitstagen zu. Als Arbeitstage
---
> länger als 30 Wochen seine Tätigkeit nicht erbringt.
> § 7 Urlaub
> (1) Dem Geschäftsführer steht kalenderjährlich ein Urlaub von 50 Arbeitstagen zu. Als Arbeitstage
85c109,114
< (3) Im Kalenderjahr nicht genommener Urlaub wird auf das Folgejahr übertragen. Wird der übertragene Urlaub nicht bis spätestens zum 30. Dezember des Folgejahres genommen, so verfällt dieser
---
> 
> 4
> 
> Vertrag 1
> 
> (3) Im Kalenderjahr nicht genommener Urlaub wird auf das Folgejahr übertragen. Wird der übertragene Urlaub nicht bis spätestens zum 24. Dezember des Folgejahres genommen, so verfällt dieser
87,95d115
< § 7 Nebentätigkeiten
< (1) Der Geschäftsführer wird ausschließlich für die Gesellschaft tätig. Die Aufnahme oder Fortführung einer weiteren selbständigen oder unselbständigen Tätigkeit sowie die Übernahme von Ehrenämtern und Aufsichtsratsmandaten bedürfen der vorherigen schriftlichen Zustimmung der
< Gesellschaft, sofern die Tätigkeit entgeltlich erfolgt oder üblicherweise nur gegen Entgelt übernommen wird oder die Leistungsfähigkeit des Geschäftsführers einschränkt. Die Gesellschaft
< wird die Zustimmung erteilen, sofern die weitere Tätigkeit die Interessen der Gesellschaft nicht
< nachteilig berührt. Die Zustimmung kann jederzeit widerrufen werden, wenn dies betriebliche
< Belange der Gesellschaft erfordern. Jede Veränderung der Art oder des Umfangs der Nebentätigkeit ist der Gesellschaft durch den Geschäftsführer unverzüglich anzuzeigen.
< (2) Auf Verlangen der Gesellschaft wird der Geschäftsführer Aufsichtsrats-, Beirats- oder Verwaltungsratsmandate sowie Ehrenämter und sonstige Ämter übernehmen, sofern dies im Interesse
< der Gesellschaft erforderlich und dem Geschäftsführer zumutbar ist. Im Falle der Beendigung
< dieses Vertrages hat der Geschäftsführer diese Mandate und Ehrenämter auf Verlangen der Gesellschaft niederzulegen, soweit dies gesetzlich möglich ist und sich der Geschäftsführer hierdurch nicht schadensersatzpflichtig macht.
102,105d121
< 4
< 
< Vertrag 1
< 
119,123c135,142
< die Gesellschaft Zahlung einer Vertragsstrafe in Höhe von € 5.000 verlangen. Besteht die Verletzungshandlung in der Eingehung eines Dauerschuldverhältnisses (z.B. auf Grund eines Arbeitsoder Dienstvertrages), in dem Betreiben eines Wettbewerbsunternehmens oder in der kapitalmäßigen Beteiligung an einem solchen Unternehmen, wird die Vertragsstrafe für jeden angefangenen Monat, in dem die konkrete Verletzungshandlung besteht, neu verwirkt (Dauerverstoß).
< Mehrere Verletzungshandlungen lösen jeweils gesonderte Vertragsstrafen aus, gegebenenfalls
< auch mehrfach innerhalb eines Monats. Erfolgen dagegen einzelne Verletzungshandlungen im
< Rahmen eines Dauerverstoßes, sind sie von der für den Dauerverstoß verwirkten Vertragsstrafe
< umfasst.
---
> die Gesellschaft Zahlung einer Vertragsstrafe in Höhe von € 1.000.000 verlangen. Besteht die
> Verletzungshandlung in der Eingehung eines Dauerschuldverhältnisses (z.B. auf Grund eines Arbeits- oder Dienstvertrages), in dem Betreiben eines Wettbewerbsunternehmens oder in der kapitalmäßigen Beteiligung an einem solchen Unternehmen, wird die Vertragsstrafe für jeden angefangenen Monat, in dem die konkrete Verletzungshandlung besteht, neu verwirkt (Dauerverstoß). Mehrere Verletzungshandlungen lösen jeweils gesonderte Vertragsstrafen aus, gegebenenfalls auch mehrfach innerhalb eines Monats. Erfolgen dagegen einzelne Verletzungshandlungen
> im Rahmen eines Dauerverstoßes, sind sie von der für den Dauerverstoß verwirkten Vertragsstrafe umfasst.
> 
> 5
> 
> Vertrag 1
> 
126c145,184
< § 10 Unterlagen und Gegenstände
---
> § 10 Nachträgliches Wettbewerbsverbot
> (1) Dem Geschäftsführer ist es nicht gestattet, für die Dauer von 24 Monaten nach dem Ende seines
> Arbeitsverhältnisses in selbständiger, unselbständiger oder anderer Weise für Dritte tätig zu werden, die mit der Gesellschaft in unmittelbarem oder mittelbaren Wettbewerb stehen. Ebenso ist
> es dem Geschäftsführer nicht gestattet, während der Laufzeit des Wettbewerbsverbotes ein solches Unternehmen zu errichten, zu erwerben oder sich hieran unmittelbar oder mittelbar zu beteiligen.
> (2) Das Wettbewerbsverbot wirkt auch im Hinblick auf die mit der Gesellschaft jetzt und in Zukunft
> verbundenen Unternehmen.
> (3) Das Wettbewerbsverbot gilt auch für und gegen einen Rechtsnachfolger der Gesellschaft. Bei
> einer Veräußerung des Betriebes geht es auf den Erwerber über. Der Geschäftsführer erklärt sich
> mit dem Übergang der Rechte aus dieser Vereinbarung auf einen etwaigen Rechtsnachfolger ausdrücklich einverstanden.
> (4) Für die Dauer des Wettbewerbsverbots zahlt die Gesellschaft eine Karenzentschädigung, die für
> jeden Monat des Verbots der Hälfte der vom Geschäftsführer zuletzt bezogenen vertragsmäßigen
> Leistungen der Gesellschaft entspricht. Eine Karenzentschädigung ist jedoch nicht zu zahlen
> während der Absolvierung des Wehrdienstes, Zivildienstes oder der Verbüßung einer Freiheitsstrafe.
> (5) Anderweitigen Erwerb muss sich der Geschäftsführer gem. § 74 c HGB auf die Entschädigung
> anrechnen lassen. Der Geschäftsführer hat unaufgefordert mitzuteilen, ob und in welcher Höhe
> er neben der Karenzentschädigung Vergütungen aus anderweitiger Verwertung seiner Arbeitskraft bezieht. Auf Verlangen sind die Angaben durch Vorlage prüfbarer Unterlagen zu belegen.
> (6) § 10 Abs. 3 dieses Vertrages gilt für das nachvertragliche Wettbewerbsverbot entsprechend.
> (7) Das nachvertragliche Wettbewerbsverbot tritt nicht in Kraft, wenn dieser Dienstvertrag aufgrund
> Eintritts des Geschäftsführers in den Ruhestand, wegen Invalidität des Geschäftsführers oder
> nach Erreichen des gesetzlichen Renteneintrittsalters endet. Das Wettbewerbsverbot und die Verpflichtung zur Zahlung einer Karenzentschädigung nach § 11 Abs. 5 enden in jedem Fall mit
> Eintritt des zuletzt genannten Zeitpunkts.
> (8) Die Gesellschaft kann zudem vor Ende dieses Dienstvertrags durch schriftliche Erklärung auf
> das nachvertragliche Wettbewerbsverbot mit der Wirkung verzichten, dass sie mit Ablauf von
> sechs Monaten seit der Erklärung von der Verpflichtung frei wird, die Karenzentschädigung zu
> zahlen.
> (9) Im Übrigen gelten die §§ 74 ff. HGB entsprechend.
> 
> 6
> 
> Vertrag 1
> 
> § 11 Gewerbliche Schutzrechte
> (1) Der Geschäftsführer räumt der Gesellschaft alle Rechte an den von ihm nach diesem Geschäftsführerdienstvertrag erzielten Arbeitsergebnissen ein, soweit diese nach den gesetzlichen Vorschriften übertragbar sind. Die Rechte stehen ausschließlich und ohne zeitliche, örtliche oder
> sachliche Beschränkung der Gesellschaft zu.
> (2) Der Geschäftsführer räumt der Gesellschaft ein ausschließliches, zeitlich, örtlich und sachlich
> unbeschränktes Nutzungsrecht an den unter das Urheberrecht fallenden Arbeitsergebnissen ein,
> soweit diese aus der Tätigkeit des Geschäftsführers für die Gesellschaft entstehen oder maßgeblich auf Erfahrungen oder Arbeiten für die Gesellschaft oder dabei erlangten Unterlagen beruhen.
> (3) Die Vereinbarungen nach Abs. 1 bis 2 behalten auch nach Beendigung des Vertragsverhältnisses
> ihre Gültigkeit.
> § 12 Unterlagen und Gegenstände
133,137d190
< 
< 5
< 
< Vertrag 1
< 
139,140c192,193
< § 11 Vertragslaufzeit
< (1) Dieser Vertrag tritt am 3. Oktober 2019 in Kraft und ist auf unbestimmte Zeit geschlossen.
---
> § 13 Vertragslaufzeit
> (1) Dieser Vertrag tritt am 9. September 2019 in Kraft und ist auf unbestimmte Zeit geschlossen.
144c197,198
< (3) Der Vertrag kann von beiden Seiten mit einer Kündigungsfrist von sechs Monaten zum Monatsende gekündigt werden.
---
> (3) Der Vertrag kann von beiden Seiten mit einer Kündigungsfrist von 12 Monaten zum Monatsende
> gekündigt werden.
147a202,206
> 
> 7
> 
> Vertrag 1
> 
151c210
< § 12 Zusicherung
---
> § 14 Zusicherung
153c212
< § 13 Schlussbestimmungen
---
> § 15 Schlussbestimmungen
160,164d218
< 
< 6
< 
< Vertrag 1
< 
171c225
< (6) Ausschließlicher Gerichtsstand für alle Streitigkeiten aus oder im Zusammenhang mit dieser Vereinbarung oder ihrer Wirksamkeit ist Göttingen.
---
> (6) Ausschließlicher Gerichtsstand für alle Streitigkeiten aus oder im Zusammenhang mit dieser Geheimhaltungsvereinbarung oder ihrer Wirksamkeit ist Hannover.
173c227
< 7
---
> 8

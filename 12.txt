3,4c3,5
< Frisöre Kaulitz GmbH, Bergstraße 17, 37075 Göttingen,
< vertreten durch Ann-Kathrin Kaulitz
---
> Immobilien Verwaltung Göttingen GmbH, Zur Mühle
> 2, 37077 Göttingen, vertreten durch Matthias Michaelsen,
> dieser vertreten durch Leon Michaelsen
8c9
< Vivien Böck, wohnhaft in Göthe-Straße 98, 37073 Göttingen
---
> Friedrich Brune, wohnhaft in Burgstraße 1, 37085 Göttingen
12,14c13
< 1
< 
< Vertrag 1
---
> Vertrag 2
17c16,17
< (1) Vivien Böck wurde mit Beschluss der Gesellschafterversammlung vom 3. Oktober 2019 mit Wirkung ab Unterzeichnung dieses Vertrages zum Geschäftsführer der Gesellschaft bestellt.
---
> (1) Friedrich Brune wurde mit Beschluss der Gesellschafterversammlung vom 2. September 2019
> mit Wirkung ab Unterzeichnung dieses Vertrages zum Geschäftsführer der Gesellschaft bestellt.
24a25,28
> (5) Der Geschäftsführer ist nach Maßgabe dieses Dienstvertrags für den Bereich Finanzen zuständig
> und verantwortlich. Die Gesellschaft behält sich vor, die Zuständigkeit und Verantwortlichkeit
> zu erweitern, zu vermindern und auch zu ändern. Die Gesellschaft ist auch berechtigt, weitere
> Geschäftsführer zu bestellen.
28c32
< (2) Arbeitsort ist Bergstraße 17, 37075 Göttingen.
---
> (2) Arbeitsort ist Zur Mühle 2, 37077 Göttingen.
31,41c35,42
< von brutto € 18.000
< (in Worten: € Achtzehntausend) zahlbar in zwölf gleichen Raten zum Monatsende.
< (2) Darüber hinaus erhält der Geschäftsführer eine Tantieme in Höhe von brutto
< 
< 25 ‰
< (in Worten: ‰ Fünfundzwanzig)
< des sich aus dem Jahresabschluss ergebenden Jahresgewinns aus der gewöhnlichen Geschäftstätigkeit der Gesellschaft ohne Berücksichtigung von Zinsen, Steuern, Steuerrückerstattungen und
< Veräußerungserlösen aus der Veräußerung von Grundstücken, Gebäuden und wesentlichem Anlagevermögen. Die Tantieme ist begrenzt auf maximal 3/12 des Jahresgehalts gemäß § 3 Abs. 1
< und wird nach Ablauf eines Monats nach Feststellung des Jahresabschlusses für das vorangegangene Geschäftsjahr nach Abzug von Steuern und evtl. Sozialabgaben zur Zahlung fällig. Beginnt
< oder endet der Dienstvertrag während der Dauer des Geschäftsjahres, so erhält er die Tantieme
< anteilig der innerhalb des Geschäftsjahres zurückgelegten Dienstzeit.
---
> von brutto € 60.000
> (in Worten: € Sechzigtausend) zahlbar in zwölf gleichen Raten zum Monatsende.
> (2) Über das Grundgehalt hinaus kann die Gesellschaft leistungsabhängige Bonuszahlungen und
> weitere Sonderleistungen gewähren. Diese Sonderleistungen sind freiwillig. Auch bei mehrmaliger Leistung entsteht kein Anspruch auf Leistung für die Zukunft. Die Gesellschaft behält sich
> vor, einzelne Leistungen nicht weiter zu gewähren oder deren Inhalt zu ändern. Etwaige auf die
> Sonderleistungen anfallende Steuern trägt der Geschäftsführer.
> (3) Mit der Zahlung des Grundgehaltes sind alle Leistungen des Geschäftsführers nach diesem Vertrag abgegolten. Dies gilt insbesondere für Arbeiten außerhalb der üblichen Bürozeiten, Übernahme von weiteren Funktionen innerhalb der Gesellschaft bzw. verbundener Unternehmen einschließlich etwaiger Aufsichtsratsmandate, Diensterfindungen und urheberrechtlicher Werke.
> (4) Im Falle von Überzahlungen kann sich der Geschäftsführer nicht auf den Wegfall der Bereicherung gemäß § 818 Abs. 3 BGB berufen.
45c46
< Vertrag 1
---
> Vertrag 2
47,54c48,62
< (3) Über das Grundgehalt hinaus kann die Gesellschaft leistungsabhängige Bonuszahlungen und
< weitere Sonderleistungen gewähren. Diese Sonderleistungen sind freiwillig. Auch bei mehrmaliger Leistung entsteht kein Anspruch auf Leistung für die Zukunft. Die Gesellschaft behält sich
< vor, einzelne Leistungen nicht weiter zu gewähren oder deren Inhalt zu ändern. Etwaige auf die
< Sonderleistungen anfallende Steuern trägt der Geschäftsführer.
< (4) Mit der Zahlung des Grundgehaltes sind alle Leistungen des Geschäftsführers nach diesem Vertrag abgegolten. Dies gilt insbesondere für Arbeiten außerhalb der üblichen Bürozeiten, Übernahme von weiteren Funktionen innerhalb der Gesellschaft bzw. verbundener Unternehmen einschließlich etwaiger Aufsichtsratsmandate, Diensterfindungen und urheberrechtlicher Werke.
< (5) Im Falle von Überzahlungen kann sich der Geschäftsführer nicht auf den Wegfall der Bereicherung gemäß § 818 Abs. 3 BGB berufen.
< (6) Über die Höhe der Vergütung haben die Parteien Stillschweigen zu bewahren.
< § 4 Erstattung von Kosten
---
> (5) Über die Höhe der Vergütung haben die Parteien Stillschweigen zu bewahren.
> § 4 Dienstfahrzeug
> (1) Der Geschäftsführer erhält ein Dienstfahrzeug, welches er auch privat nutzen darf, vorausgesetzt,
> dass hierfür ein gesonderter Kfz-Überlassungsvertrag abgeschlossen wurde, der diesem Dienstvertrag als Anlage 1 beigefügt ist. Die Nutzung des Dienstfahrzeugs erfolgt ausschließlich entsprechend den Regelungen dieses Dienstvertrags und des Kfz-Überlassungsvertrags. Der geldwerte Vorteil der Privatnutzung liegt deutlich unter 25% der Vergütung des Geschäftsführers nach
> diesem Vertrag.
> (2) Die private Nutzung kann von der Gesellschaft widerrufen werden, wenn der Geschäftsführer
> das Dienstfahrzeug vertragswidrig benutzt oder wenn der Dienstvertrag gekündigt worden ist
> und wenn die Gesellschaft den Geschäftsführer berechtigt von seiner Verpflichtung zur Tätigkeit
> für die Gesellschaft freigestellt oder suspendiert hat. Ein Anspruch des Geschäftsführers wegen
> des Entzugs der privaten Nutzung besteht in diesen Fällen nicht.
> (3) Die Versteuerung des mit der privaten Nutzung verbundenen geldwerten Vorteils erfolgt nach
> den jeweils geltenden steuerlichen Regelungen im Rahmen des Lohnsteuerabzugsverfahrens und
> erfolgt zu Lasten des Geschäftsführers.
> (4) Im Übrigen gelten die Regelungen des Kfz-Überlassungsvertrags.
> § 5 Erstattung von Kosten
59,60c67,71
< ersten Klasse erstattet, bei Nutzung des Flugzeugs Tickets der Economy-Klasse und der Business-Klasse für Flüge über sechs Stunden Dauer.
< § 5 Arbeitsverhinderung, Erkrankung
---
> ersten Klasse erstattet, bei Nutzung des Flugzeugs Tickets der Economy-Klasse und der Business-Klasse für Flüge über sechs Stunden Dauer. Leihwagen können in der Größenordnung des
> Firmenwagens angemietet werden.
> (3) Die Gesellschaft zahlt an den Geschäftsführer monatlich einen Zuschuss in Höhe von € 60 brutto
> für seinen privaten Telefonanschluss, den er auch geschäftlich nutzt.
> § 6 Arbeitsverhinderung, Erkrankung
63,72c74,75
< (2) Im Falle einer Arbeitsunfähigkeit wegen Krankheit ist der Geschäftsführer verpflichtet, der Gesellschaft eine ärztliche Bescheinigung über die Arbeitsverhinderung und ihre voraussichtliche
< Dauer vorzulegen. Dauert die Arbeitsunfähigkeit länger als in der Bescheinigung angegeben, ist
< der Geschäftsführer verpflichtet, innerhalb von drei Tagen eine neue ärztliche Bescheinigung
< einzureichen. Auch in diesem Fall muss der Geschäftsführer der Gesellschaft unverzüglich unterrichten, falls die Dauer der Krankheit die in der Folgebescheinigung angegebene Dauer überschreitet.
< (3) Wird der Geschäftsführer an seiner Arbeitsleistung infolge Krankheit verhindert, ohne dass ihn
< 
< ein Verschulden trifft, so behält er seinen Anspruch auf Zahlung des Grundgehalts für die Zeit
< der Arbeitsunfähigkeit bis zur Dauer von sechs Monaten, längstens jedoch bis zum Ende dieses
< Vertrages.
< 
---
> (2) Im Falle einer Arbeitsunfähigkeit wegen Krankheit ist der Geschäftsführer verpflichtet, spätestens bis nach drei Tagen der Gesellschaft eine ärztliche Bescheinigung über die Arbeitsverhinderung und ihre voraussichtliche Dauer vorzulegen. Dauert die Arbeitsunfähigkeit länger als in der
> Bescheinigung angegeben, ist der Geschäftsführer verpflichtet, innerhalb von drei Tagen eine
75c78
< Vertrag 1
---
> Vertrag 2
77,81c80,87
< Die Ansprüche auf Zahlung der Tantieme werden entsprechend pro rata temporis gekürzt, wenn
< der Geschäftsführer wegen Krankheit oder sonstiger persönlicher Verhinderung im Kalenderjahr
< länger als zehn Wochen seine Tätigkeit nicht erbringt.
< § 6 Urlaub
< (1) Dem Geschäftsführer steht kalenderjährlich ein Urlaub von 20 Arbeitstagen zu. Als Arbeitstage
---
> neue ärztliche Bescheinigung einzureichen. Auch in diesem Fall muss der Geschäftsführer der
> Gesellschaft unverzüglich unterrichten, falls die Dauer der Krankheit die in der Folgebescheinigung angegebene Dauer überschreitet.
> (3) Wird der Geschäftsführer an seiner Arbeitsleistung infolge Krankheit verhindert, ohne dass ihn
> ein Verschulden trifft, so behält er seinen Anspruch auf Zahlung des Grundgehalts für die Zeit
> der Arbeitsunfähigkeit bis zur Dauer von 10 Monaten, längstens jedoch bis zum Ende dieses
> Vertrages.
> § 7 Urlaub
> (1) Dem Geschäftsführer steht kalenderjährlich ein Urlaub von 30 Arbeitstagen zu. Als Arbeitstage
85c91
< (3) Im Kalenderjahr nicht genommener Urlaub wird auf das Folgejahr übertragen. Wird der übertragene Urlaub nicht bis spätestens zum 30. Dezember des Folgejahres genommen, so verfällt dieser
---
> (3) Im Kalenderjahr nicht genommener Urlaub wird auf das Folgejahr übertragen. Wird der übertragene Urlaub nicht bis spätestens zum 1. Dezember des Folgejahres genommen, so verfällt dieser
87c93
< § 7 Nebentätigkeiten
---
> § 8 Nebentätigkeiten
96c102
< § 8 Verschwiegenheit
---
> § 9 Verschwiegenheit
98,101c104,105
< zählen insbesondere: Projektbeschreibungen und laufende Angebote, Know-how der Gesellschaft, Kenntnisse über die von der Gesellschaft angewandten Methoden, Preisbildung und Gewinnmargen, Zeichnungen, Testergebnisse, Muster, Businesspläne, Erfindungen sowie Namen
< der Kunden und Lieferanten der Gesellschaft.
< (2) Darüber hinaus ist der Geschäftsführer verpflichtet, Stillschweigen über solche Informationen zu
< bewahren, die ihm von den Gesellschaftern, Organmitgliedern oder Mitarbeitern der Gesellschaft
---
> zählen insbesondere: Projektbeschreibungen und laufende Angebote, Know-how der Gesell-
> 
104c108
< Vertrag 1
---
> Vertrag 2
105a110,113
> schaft, Kenntnisse über die von der Gesellschaft angewandten Methoden, Preisbildung und Gewinnmargen, Zeichnungen, Testergebnisse, Muster, Businesspläne, Erfindungen sowie Namen
> der Kunden und Lieferanten der Gesellschaft.
> (2) Darüber hinaus ist der Geschäftsführer verpflichtet, Stillschweigen über solche Informationen zu
> bewahren, die ihm von den Gesellschaftern, Organmitgliedern oder Mitarbeitern der Gesellschaft
111c119
< § 9 Wettbewerbsverbot
---
> § 10 Wettbewerbsverbot
119,123c127,128
< die Gesellschaft Zahlung einer Vertragsstrafe in Höhe von € 5.000 verlangen. Besteht die Verletzungshandlung in der Eingehung eines Dauerschuldverhältnisses (z.B. auf Grund eines Arbeitsoder Dienstvertrages), in dem Betreiben eines Wettbewerbsunternehmens oder in der kapitalmäßigen Beteiligung an einem solchen Unternehmen, wird die Vertragsstrafe für jeden angefangenen Monat, in dem die konkrete Verletzungshandlung besteht, neu verwirkt (Dauerverstoß).
< Mehrere Verletzungshandlungen lösen jeweils gesonderte Vertragsstrafen aus, gegebenenfalls
< auch mehrfach innerhalb eines Monats. Erfolgen dagegen einzelne Verletzungshandlungen im
< Rahmen eines Dauerverstoßes, sind sie von der für den Dauerverstoß verwirkten Vertragsstrafe
< umfasst.
---
> die Gesellschaft Zahlung einer Vertragsstrafe in Höhe von € 25.000 verlangen. Besteht die Verletzungshandlung in der Eingehung eines Dauerschuldverhältnisses (z.B. auf Grund eines Arbeits- oder Dienstvertrages), in dem Betreiben eines Wettbewerbsunternehmens oder in der kapitalmäßigen Beteiligung an einem solchen Unternehmen, wird die Vertragsstrafe für jeden angefangenen Monat, in dem die konkrete Verletzungshandlung besteht, neu verwirkt (Dauerverstoß). Mehrere Verletzungshandlungen lösen jeweils gesonderte Vertragsstrafen aus, gegebenenfalls auch mehrfach innerhalb eines Monats. Erfolgen dagegen einzelne Verletzungshandlungen
> im Rahmen eines Dauerverstoßes, sind sie von der für den Dauerverstoß verwirkten Vertragsstrafe umfasst.
126c131,183
< § 10 Unterlagen und Gegenstände
---
> § 11 Nachträgliches Wettbewerbsverbot
> (1) Dem Geschäftsführer ist es nicht gestattet, für die Dauer von 12 Monaten nach dem Ende seines
> Arbeitsverhältnisses in selbständiger, unselbständiger oder anderer Weise für Dritte tätig zu werden, die mit der Gesellschaft in unmittelbarem oder mittelbaren Wettbewerb stehen. Ebenso ist
> 
> 5
> 
> Vertrag 2
> 
> es dem Geschäftsführer nicht gestattet, während der Laufzeit des Wettbewerbsverbotes ein solches Unternehmen zu errichten, zu erwerben oder sich hieran unmittelbar oder mittelbar zu beteiligen.
> (2) Das Wettbewerbsverbot wirkt auch im Hinblick auf die mit der Gesellschaft jetzt und in Zukunft
> verbundenen Unternehmen.
> (3) Das Wettbewerbsverbot gilt auch für und gegen einen Rechtsnachfolger der Gesellschaft. Bei
> einer Veräußerung des Betriebes geht es auf den Erwerber über. Der Geschäftsführer erklärt sich
> mit dem Übergang der Rechte aus dieser Vereinbarung auf einen etwaigen Rechtsnachfolger ausdrücklich einverstanden.
> (4) Für die Dauer des Wettbewerbsverbots zahlt die Gesellschaft eine Karenzentschädigung, die für
> jeden Monat des Verbots der Hälfte der vom Geschäftsführer zuletzt bezogenen vertragsmäßigen
> Leistungen der Gesellschaft entspricht. Eine Karenzentschädigung ist jedoch nicht zu zahlen
> während der Absolvierung des Wehrdienstes, Zivildienstes oder der Verbüßung einer Freiheitsstrafe.
> (5) Anderweitigen Erwerb muss sich der Geschäftsführer gem. § 74 c HGB auf die Entschädigung
> anrechnen lassen. Der Geschäftsführer hat unaufgefordert mitzuteilen, ob und in welcher Höhe
> er neben der Karenzentschädigung Vergütungen aus anderweitiger Verwertung seiner Arbeitskraft bezieht. Auf Verlangen sind die Angaben durch Vorlage prüfbarer Unterlagen zu belegen.
> (6) § 10 Abs. 3 dieses Vertrages gilt für das nachvertragliche Wettbewerbsverbot entsprechend.
> (7) Das nachvertragliche Wettbewerbsverbot tritt nicht in Kraft, wenn dieser Dienstvertrag aufgrund
> Eintritts des Geschäftsführers in den Ruhestand, wegen Invalidität des Geschäftsführers oder
> nach Erreichen des gesetzlichen Renteneintrittsalters endet. Das Wettbewerbsverbot und die Verpflichtung zur Zahlung einer Karenzentschädigung nach § 11 Abs. 5 enden in jedem Fall mit
> Eintritt des zuletzt genannten Zeitpunkts.
> (8) Die Gesellschaft kann zudem vor Ende dieses Dienstvertrags durch schriftliche Erklärung auf
> das nachvertragliche Wettbewerbsverbot mit der Wirkung verzichten, dass sie mit Ablauf von
> sechs Monaten seit der Erklärung von der Verpflichtung frei wird, die Karenzentschädigung zu
> zahlen.
> (9) Im Übrigen gelten die §§ 74 ff. HGB entsprechend.
> § 12 Gewerbliche Schutzrechte
> (1) Der Geschäftsführer räumt der Gesellschaft alle Rechte an den von ihm nach diesem Geschäftsführerdienstvertrag erzielten Arbeitsergebnissen ein, soweit diese nach den gesetzlichen Vorschriften übertragbar sind. Die Rechte stehen ausschließlich und ohne zeitliche, örtliche oder
> sachliche Beschränkung der Gesellschaft zu.
> (2) Der Geschäftsführer räumt der Gesellschaft ein ausschließliches, zeitlich, örtlich und sachlich
> unbeschränktes Nutzungsrecht an den unter das Urheberrecht fallenden Arbeitsergebnissen ein,
> 
> 6
> 
> Vertrag 2
> 
> soweit diese aus der Tätigkeit des Geschäftsführers für die Gesellschaft entstehen oder maßgeblich auf Erfahrungen oder Arbeiten für die Gesellschaft oder dabei erlangten Unterlagen beruhen.
> (3) Das Nutzungsrecht nach Abs. 2 umfasst alle Nutzungsarten, seien sie bekannt oder noch unbekannt, jetzt oder zukünftig. Dazu gehört auch das Recht, das Werk zu bearbeiten, zu verändern,
> zu vervielfältigen, zu veröffentlichen und zu verwerten.
> (4) Eine Verpflichtung zur Autorernennung besteht nicht. Das Zugangsrecht des Geschäftsführers zu
> den von ihm geschaffenen Werken bzw. Werkteilen sowie sonstigen Arbeitsergebnissen und das
> Recht auf Herausgabe einer Autorenkopie werden ausgeschlossen.
> (5) Die Gesellschaft ist ferner ohne gesonderte Zustimmung befugt, die Rechte an den Arbeitsergebnissen ganz oder teilweise auf Dritte zu übertragen oder Dritten Nutzungsrechte einzuräumen.
> (6) Die Einräumung der Rechte nach Abs. 1 bis 5 ist mit der Vergütung des Geschäftsführers gemäß
> § 3 dieses Geschäftsführerdienstvertrages abgegolten.
> (7) Die Vereinbarungen nach Abs. 1 bis 6 behalten auch nach Beendigung des Vertragsverhältnisses
> ihre Gültigkeit.
> § 13 Unterlagen und Gegenstände
133,137d189
< 
< 5
< 
< Vertrag 1
< 
139,141c191,193
< § 11 Vertragslaufzeit
< (1) Dieser Vertrag tritt am 3. Oktober 2019 in Kraft und ist auf unbestimmte Zeit geschlossen.
< 
---
> § 14 Vertragslaufzeit
> (1) Dieser Vertrag beginnt am 2. September 2019 und wird für die Dauern von fünf Jahren geschlossen. Er verlängert sich jeweils um 2 Jahre, wenn er nicht mit einer Frist von 6 Monaten vor dem
> jeweiligen Vertragsende gekündigt wird.
144c196,201
< (3) Der Vertrag kann von beiden Seiten mit einer Kündigungsfrist von sechs Monaten zum Monatsende gekündigt werden.
---
> (3) Der Vertrag kann von beiden Seiten mit einer Kündigungsfrist von sechs zum Monatsende gekündigt werden.
> 
> 7
> 
> Vertrag 2
> 
151c208
< § 12 Zusicherung
---
> § 15 Zusicherung
153c210
< § 13 Schlussbestimmungen
---
> § 16 Schlussbestimmungen
160,164d216
< 
< 6
< 
< Vertrag 1
< 
173c225
< 7
---
> 8
